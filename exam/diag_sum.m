function q=diag_sum(A)
[x,y]=size(A);
a=min(x,y);
c=(a+1)/2;
A=A([1:a],[1:a]);
B=flip(A);
if mod(a,2)==0
     q=sum(diag(A))+sum(diag(B));
else
     q=sum(diag(A))+sum(diag(B))-A(c,c);
end
end